package Control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Models.Comanda;

public class ComandaDAO {
	
	public ArrayList<Comanda> comenzi = new ArrayList<Comanda>();
	Comanda o = new Comanda();

	public void addComanda(Comanda comanda)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			PreparedStatement prepSt = myConn.prepareStatement("insert into comanda(Produs_idProdus,Client_idClient,total) values(?,?,?)");
			
			//prepSt.setString(1, String.valueOf(comanda.idComanda));
			prepSt.setString(1, String.valueOf(comanda.produs));
			prepSt.setString(2, String.valueOf(comanda.client));
			prepSt.setString(3, comanda.total);
			
			prepSt.executeUpdate();
			
		} catch (SQLException e) {
			System.out.println("Clientul/Produsul nu exista");
			e.printStackTrace();
		}
	}
	
	public Comanda getComanda(int id)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			PreparedStatement prepSt = myConn.prepareStatement("select * from comanda where idComanda = ?");
			
			prepSt.setString(1, String.valueOf(id));
			
			ResultSet myRs = prepSt.executeQuery();
			
			while(myRs.next())
			{
				o = new Comanda(Integer.parseInt(myRs.getString("idComanda")),Integer.parseInt(myRs.getString("Produs_idProdus")),Integer.parseInt(myRs.getString("Client_idClient")),myRs.getString("total"));
				return o;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void getComenzi()
	{
		try 
		{
		comenzi.clear();
		
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("select * from comanda");
		ResultSet myRs = prepSt.executeQuery();
		
		while(myRs.next())
		{
			comenzi.add(new Comanda(Integer.parseInt(myRs.getString("idComanda")),Integer.parseInt(myRs.getString("Produs_idProdus")),Integer.parseInt(myRs.getString("Client_idClient")),myRs.getString("total")));
		}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void updateComanda(Comanda comanda)
	{
		try 
		{
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("update comanda set nume = ?, cantitate = ? , pret = ? where idComanda = ?");
		
		
		prepSt.setString(1, String.valueOf(comanda.produs));
		prepSt.setString(2, String.valueOf(comanda.client));
		prepSt.setString(3, comanda.total);
		prepSt.setString(4, String.valueOf(comanda.idComanda));
		
		prepSt.executeUpdate();

		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void deleteComanda(int id)
	{
		try 
		{	
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("delete from comanda where idComanda = ?");
		
		prepSt.setString(1, String.valueOf(id));
		
		prepSt.executeUpdate();
		
		
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public int getComandaId()
	{
		try 
		{	
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("select MAX(idComanda) from comanda");
		
		ResultSet myRs = prepSt.executeQuery();
		
		int i = 0;
		while(myRs.next())
		{
			i = Integer.parseInt(myRs.getString("MAX(idComanda)"));
		}
		
		return i;
		
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		return 0;
	}
	
}
