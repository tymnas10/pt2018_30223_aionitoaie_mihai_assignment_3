package Control;
import Models.*;
import View.GUI;
import java.sql.*;
//import com.itextpdf.text.*;
import javax.swing.JFrame;


public class DB {
	
	String[][] client,produs,comanda;
	public ClientDAO c = new ClientDAO();
	public ProdusDAO p = new ProdusDAO();
	public ComandaDAO o = new ComandaDAO();
	
	public void addClientDB(String nume,String tel,String adr)
	{
		Client client = new Client(1,nume,tel,adr);
		c.addClient(client);
		
	}
	
	public void addProdusDB(String nume,String cant,String pret)
	{
		Produs produs = new Produs(1,nume,cant,pret);
		p.addProdus(produs);
	}
	
	public void addComandaDB(int idP,int idC,String total)
	{
		Comanda comanda = new Comanda(1,idP,idC,total);
		o.addComanda(comanda);
	}
	
	public void removeClientDB(int id)
	{
		c.deleteClient(id);
	}
	
	public void removeProdusDB(int id)
	{
		p.deleteProdus(id);
	}
	
	public void removeComandaDB(int id)
	{
		o.deleteComanda(id);
	}
	
	public void updateClientDB(int id,String nume,String tel,String adr)
	{
		Client client = new Client(id,nume,tel,adr);
		c.updateClient(client);
	}
	
	public void updateProdusDB(int id,String nume,String cant,String pret)
	{
		Produs produs = new Produs(id,nume,cant,pret);
		p.updateProdus(produs);
	}
	
	public void updateComandaDB(int id,int idP,int idC,String total)
	{
		Comanda comanda = new Comanda(id,idP,idC,total);
		o.updateComanda(comanda);
	}
	
	public int getComandaId()
	{
		return o.getComandaId();
	}
	
	public DB()
	{
			
			//Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			/*
			PreparedStatement prepSt = myConn.prepareStatement("insert into client(idClient,nume,telefon,adresa) values(?,?,?,?)");
			prepSt.setString(1, "2");
			prepSt.setString(2, "vasile");
			prepSt.setString(3, "0745");
			prepSt.setString(4, "Galati");
			
			prepSt.executeUpdate();
			

			PreparedStatement stm = myConn.prepareStatement("select * from client");
			ResultSet myRs = stm.executeQuery();
			
			while(myRs.next())
			{
				System.out.println(myRs.getString("idClient") +" "+ myRs.getString("nume") +" "+ myRs.getString("telefon") +" "+ myRs.getString("adresa"));
			}
			
*/			//test client
			
			//Client client = new Client(2,"testtt","06555","clujjj");
			//ClientDAO c = new ClientDAO();
			//c.addClient(client);
			//c.deleteClient(0);
			//c.updateClient(client);
			//c.getClients();
			//System.out.println(c.clienti.get(1).idClient);
			
			
			//test produs
			
			//Produs produs = new Produs(1,"mere","30","0.5");
			//ProdusDAO p = new ProdusDAO();
			//p.addProdus(produs);
			//p.deleteProdus(1);
			//p.updateProdus(produs);
			//p.getProduse();
			//System.out.println(p.produse.get(0).numeP);
			
		
		
		
	}

}
