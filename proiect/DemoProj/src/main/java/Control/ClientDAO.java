package Control;

import java.sql.*;
import java.util.*;

import Models.Client;

//one DAO per table or view

//CRUD - create retrieve update delete
public class ClientDAO {


	public ArrayList<Client> clienti = new ArrayList<Client>();
	Client c = new Client();
	
	public void addClient(Client client)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			//PreparedStatement prepSt = myConn.prepareStatement("insert into client(idClient,nume,telefon,adresa) values(?,?,?,?)");
			PreparedStatement prepSt = myConn.prepareStatement("insert into client(nume,telefon,adresa) values(?,?,?)");
			
			
			//prepSt.setString(1, String.valueOf(client.idClient));
			prepSt.setString(1, client.numeC);
			prepSt.setString(2, client.telefon);
			prepSt.setString(3, client.adresa);
			
			prepSt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Client getClient(int id)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			PreparedStatement prepSt = myConn.prepareStatement("select * from client where idClient = ?");
			
			prepSt.setString(1, String.valueOf(id));
			
			ResultSet myRs = prepSt.executeQuery();
			
			while(myRs.next())
			{
				c = new Client(Integer.parseInt(myRs.getString("idClient")),myRs.getString("nume"),myRs.getString("telefon"),myRs.getString("adresa"));
				return c;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void getClients()
	{
		try 
		{
		clienti.clear();
		
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("select * from client");
		ResultSet myRs = prepSt.executeQuery();
		
		while(myRs.next())
		{
			clienti.add(new Client(Integer.parseInt(myRs.getString("idClient")),myRs.getString("nume"),myRs.getString("telefon"),myRs.getString("adresa")));
		}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}

		
	}
	
	public void updateClient(Client client)
	{
		try 
		{
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("update client set nume = ?, telefon = ? , adresa = ? where idClient = ?");
		
		
		prepSt.setString(1, client.numeC);
		prepSt.setString(2, client.telefon);
		prepSt.setString(3, client.adresa);
		prepSt.setString(4, String.valueOf(client.idClient));
		
		prepSt.executeUpdate();

		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void deleteClient(int id)
	{
		try 
		{	
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("delete from client where idClient = ?");
		
		prepSt.setString(1, String.valueOf(id));
		
		prepSt.executeUpdate();
		
		
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
