package Control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Models.Produs;

public class ProdusDAO {

	public ArrayList<Produs> produse = new ArrayList<Produs>();
	Produs p = new Produs();
	
	public void addProdus(Produs produs)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			//PreparedStatement prepSt = myConn.prepareStatement("insert into produs(idProdus,nume,cantitate,pret) values(?,?,?,?)");
			PreparedStatement prepSt = myConn.prepareStatement("insert into produs(nume,cantitate,pret) values(?,?,?)");
			
			//prepSt.setString(1, String.valueOf(produs.idProdus));
			prepSt.setString(1, produs.numeP);
			prepSt.setString(2, produs.cantitate);
			prepSt.setString(3, produs.pret);
			
			prepSt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Produs getProdus(int id)
	{
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
			PreparedStatement prepSt = myConn.prepareStatement("select * from produs where idProdus = ?");
			
			prepSt.setString(1, String.valueOf(id));
			
			ResultSet myRs = prepSt.executeQuery();
			
			while(myRs.next())
			{
				p = new Produs(Integer.parseInt(myRs.getString("idProdus")),myRs.getString("nume"),myRs.getString("cantitate"),myRs.getString("pret"));
				return p;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void getProduse()
	{
		try 
		{
		produse.clear();
		
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("select * from produs");
		ResultSet myRs = prepSt.executeQuery();
		
		while(myRs.next())
		{
			produse.add(new Produs(Integer.parseInt(myRs.getString("idProdus")),myRs.getString("nume"),myRs.getString("cantitate"),myRs.getString("pret")));
		}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void updateProdus(Produs produs)
	{
		try 
		{
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("update produs set nume = ?, cantitate = ? , pret = ? where idProdus = ?");
		
		
		prepSt.setString(1, produs.numeP);
		prepSt.setString(2, produs.cantitate);
		prepSt.setString(3, produs.pret);
		prepSt.setString(4, String.valueOf(produs.idProdus));
		
		prepSt.executeUpdate();

		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void deleteProdus(int id)
	{
		try 
		{	
		Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/problema3", "root", "");
		PreparedStatement prepSt = myConn.prepareStatement("delete from produs where idProdus = ?");
		
		prepSt.setString(1, String.valueOf(id));
		
		prepSt.executeUpdate();
		
		
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
