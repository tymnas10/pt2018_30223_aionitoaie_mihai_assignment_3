package View;//teoretic importa modele//controller importa si model si view
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import Control.DB;
import Models.Client;
import Models.Comanda;
import Models.Produs;
import relection.TableReflection;

public class GUI extends JFrame {

	//JTable tableC,tableP,tableO;
	int temp = 0;
	
	TableReflection tr = new TableReflection();
	JFrame frame;
	JButton b1,b2,b3;
	DB test = new DB();
	DefaultTableModel modelC = new DefaultTableModel();
	final DefaultTableModel modelP = new DefaultTableModel();
	final DefaultTableModel modelO = new DefaultTableModel();
	boolean activate = true;
	
	JTable tableC = new JTable(); JTable tableP = new JTable(); JTable tableO = new JTable();
	///
	
	ArrayList<Client> cl = new ArrayList<Client>();
	ArrayList<Produs> pr = new ArrayList<Produs>();
	ArrayList<Comanda> com = new ArrayList<Comanda>();
	
	
	public GUI()
	{
		////
		final JFrame frame = new JFrame();
		//JTable tableC = new JTable(); final JTable tableP = new JTable(); final JTable tableO = new JTable();
		///
		test.c.getClients();
		test.p.getProduse();
		test.o.getComenzi();
		cl = test.c.clienti;
		pr = test.p.produse;
		com = test.o.comenzi;

		ArrayList<String> colCl = new ArrayList<String>();
		colCl = tr.displayField(new Client());

		
		String[] colC = colCl.toArray(new String[colCl.size()]);
		//String[] colP = {"Id","Nume","cantitate","pret"};
		String[] colP = tr.displayField(new Produs()).toArray(new String[tr.displayField(new Produs()).size()]);
		//String[] colO = {"Id","idClient","idProdus","total"};
		String[] colO = tr.displayField(new Comanda()).toArray(new String[tr.displayField(new Comanda()).size()]);

		modelC.setColumnIdentifiers(colC); modelP.setColumnIdentifiers(colP); modelO.setColumnIdentifiers(colO); 
		//tableC.setModel(modelC); tableP.setModel(modelP); tableO.setModel(modelO);
		////
		//modelC = tr.displayDAta(cl);
		
		
		
		//tableC.setModel(modelC);
		//tableC.setModel(tr.displayDAta(cl));
		//tableP.setModel(tr.displayDAta(pr));
		//tableO.setModel(tr.displayDAta(com)); 
		
		
		//JScrollPane pane = new JScrollPane(tableC);JScrollPane paneP = new JScrollPane(tableP);JScrollPane paneO = new JScrollPane(tableO);
		
		if(temp == 0)
		{
			tableC =  new JTable(tr.displayDAta(cl),tr.displayField(new Client()).toArray(new String[tr.displayField(new Client()).size()]));
			tableP = new JTable(tr.displayDAta(pr),tr.displayField(new Produs()).toArray(new String[tr.displayField(new Produs()).size()]));
			tableO = new JTable(tr.displayDAta(com),tr.displayField(new Comanda()).toArray(new String[tr.displayField(new Comanda()).size()]));
		}
		
		///custom table
		System.out.println();
		//JTable tablet = new JTable(tr.displayDAta(cl),tr.displayField(new Client()).toArray(new String[tr.displayField(new Client()).size()]));
		//JTable tablett = new JTable(tr.displayDAta(pr),tr.displayField(new Produs()).toArray(new String[tr.displayField(new Produs()).size()]));
		//JTable tablettt = new JTable(tr.displayDAta(com),tr.displayField(new Comanda()).toArray(new String[tr.displayField(new Comanda()).size()]));
		
		//tableC.setModel(modelC); tableP.setModel(modelP); tableO.setModel(modelO);
		tableC.setBackground(Color.GRAY);tableP.setBackground(Color.GRAY);tableO.setBackground(Color.GRAY);
		tableC.setForeground(Color.black);tableP.setForeground(Color.black);tableO.setForeground(Color.black);
		Font font = new Font("",1,20);
		tableC.setFont(font);tableP.setFont(font);tableO.setFont(font);
		tableC.setRowHeight(25);tableP.setRowHeight(25);tableO.setRowHeight(25);
		///
		//tableC =  new JTable(tr.displayDAta(cl),tr.displayField(new Client()).toArray(new String[tr.displayField(new Client()).size()]));
		//tableP = new JTable(tr.displayDAta(pr),tr.displayField(new Produs()).toArray(new String[tr.displayField(new Produs()).size()]));
		//tableO = new JTable(tr.displayDAta(com),tr.displayField(new Comanda()).toArray(new String[tr.displayField(new Comanda()).size()]));
		//add table to pane
		JScrollPane pane = new JScrollPane(tableC);JScrollPane paneP = new JScrollPane(tableP);JScrollPane paneO = new JScrollPane(tableO);
		
		pane.setBounds(0,0,300,300);paneP.setBounds(320,0,300,300);paneO.setBounds(640,0,300,300);
		frame.setLayout(null);
		////
		

		populareTabele();

		if(activate) {
		modelC.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent arg0) {
				
				int i = tableC.getSelectedRow();
				
				if(i >= 0)
					try {
					test.updateClientDB(Integer.parseInt(String.valueOf(modelC.getValueAt(i, 0))),String.valueOf(modelC.getValueAt(i, 1)),String.valueOf(modelC.getValueAt(i, 2)),String.valueOf(modelC.getValueAt(i,3)));
					//System.out.println(String.valueOf(modelC.getValueAt(i, 0)) + String.valueOf(modelC.getValueAt(i, 1)) + String.valueOf(modelC.getValueAt(i, 2)) + String.valueOf(modelC.getValueAt(i,3)));
					}catch(Exception e)
				{
						System.out.println("refresh");
				}
			}
		});
		
		modelP.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent arg0)
			{
				int i = tableP.getSelectedRow();
				
				
				if(i >= 0)
					try {
					test.updateProdusDB(Integer.parseInt(String.valueOf(modelP.getValueAt(i, 0))),String.valueOf(modelP.getValueAt(i, 1)),String.valueOf(modelP.getValueAt(i, 2)),String.valueOf(modelP.getValueAt(i,3)));
					}catch(Exception e)
				{
						System.out.println("refresh");
				}
			}
		});
		}
		/*
		modelO.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent arg0)
			{
				int i = tableO.getSelectedRow();
				
				if(i >= 0)
					test.updateComandaDB(Integer.parseInt(String.valueOf(modelO.getValueAt(i, 0))), Integer.parseInt(String.valueOf(modelO.getValueAt(i, 1))), Integer.parseInt(String.valueOf(modelO.getValueAt(i, 2))), String.valueOf(modelO.getValueAt(i, 3)));
			}
		});*/
		
		/////////adaugare in tabele///////////
		b1 = new JButton("add client"); b2 = new JButton("add produs"); b3 = new JButton("add comanda");
		b1.setBounds(400,400,100,30); b2.setBounds(400,450,100,30); b3.setBounds(400,500,120,30);
		b1.setForeground(Color.black); b2.setForeground(Color.black); b3.setForeground(Color.black);
		
		b1.addActionListener(createAction(modelC,"Nume","Telefon","adresa","Client"));
		b2.addActionListener(createAction(modelP,"Nume","Cantitate","Pret","Produs"));
		b3.addActionListener(createAction(modelO,"Id Client","Id Produs","Total","Comanda"));
		////////////////////////////////
		
		
		JButton but = new JButton("delete");
		but.setBounds(600,500,100,30); 
		frame.add(but); 

		
		but.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int i = tableC.getSelectedRow();
				int j = tableP.getSelectedRow();
				int k = tableO.getSelectedRow();
				if(i >= 0) {
					removeDinTabel(modelC,i);
					activate = false;
				}
				if(j >= 0) {
					removeDinTabel(modelP,j);
					activate = false;
				}
				if(k >= 0) {
					removeDinTabel(modelO,k);
					activate = false;
				}
				
				else
					activate = true;
					//System.out.println("delete error");
				
			}
		});;

		
		
		
		/*
		
		
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				JFrame f1 = new JFrame("Add Comanda");
				f1.setSize(400,400);
				f1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				f1.setVisible(true);
				f1.setLayout(null);
				
				final JTextField nume;
				final JTextField tel;
				final JTextField adresa;
				JLabel n,t,a;
				JButton add;
				
				nume = new JTextField(); tel = new JTextField(); adresa = new JTextField();
				n = new JLabel("Nume"); t = new JLabel("Telefon"); a = new JLabel("Adresa");
				add = new JButton("ADD");
				
				nume.setBounds(40,50,150,30); n.setBounds(40,20,100,30); n.setForeground(Color.black);
				tel.setBounds(40,150,150,30); t.setBounds(40,120,100,30); t.setForeground(Color.black);
				adresa.setBounds(40,250,150,30); a.setBounds(40,220,100,30); a.setForeground(Color.black);
			
				add.setBounds(250,200,100,30);
				add.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						String[] rowt = {nume.getText(),tel.getText(),adresa.getText()};
						modelC.addRow(rowt);
					}
				});
				
				f1.add(add);
				f1.add(nume); f1.add(tel); f1.add(adresa);
				f1.add(n); f1.add(t); f1.add(a);
			}
		});
		
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				JFrame f1 = new JFrame("Add Produs");
				f1.setSize(400,400);
				f1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				f1.setVisible(true);
				f1.setLayout(null);
				
				final JTextField nume;
				final JTextField cant;
				final JTextField pret;
				JLabel n,t,a;
				JButton add;
				
				nume = new JTextField(); cant = new JTextField(); pret = new JTextField();
				n = new JLabel("Nume"); t = new JLabel("Cantitate"); a = new JLabel("pret");
				add = new JButton("ADD");
				
				nume.setBounds(40,50,150,30); n.setBounds(40,20,100,30); n.setForeground(Color.black);
				cant.setBounds(40,150,150,30); t.setBounds(40,120,100,30); t.setForeground(Color.black);
				pret.setBounds(40,250,150,30); a.setBounds(40,220,100,30); a.setForeground(Color.black);
			
				add.setBounds(250,200,100,30);
				add.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						String[] rowt = {nume.getText(),cant.getText(),pret.getText()};
						modelP.addRow(rowt);
					}
				});
				
				f1.add(add);
				f1.add(nume); f1.add(cant); f1.add(pret);
				f1.add(n); f1.add(t); f1.add(a);
			}
		});
		
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				JFrame f1 = new JFrame("Add Comanda");
				f1.setSize(400,400);
				f1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				f1.setVisible(true);
				f1.setLayout(null);
				
				final JTextField idC;
				final JTextField idP;
				final JTextField total;
				JLabel n,t,a;
				JButton add;
				
				idC = new JTextField(); idP = new JTextField(); total = new JTextField();
				n = new JLabel("Id Client"); t = new JLabel("Id Produs"); a = new JLabel("total");
				add = new JButton("ADD");
				
				idC.setBounds(40,50,150,30); n.setBounds(40,20,100,30); n.setForeground(Color.black);
				idP.setBounds(40,150,150,30); t.setBounds(40,120,100,30); t.setForeground(Color.black);
				total.setBounds(40,250,150,30); a.setBounds(40,220,100,30); a.setForeground(Color.black);
			
				add.setBounds(250,200,100,30);
				add.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						String[] rowt = {idC.getText(),idP.getText(),total.getText()};

						modelO.addRow(rowt);


						
						
						try {
							Document document = new Document();
							PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\user\\Desktop\\BDpb3\\pdftest\\test" + test.getComandaId() + ".pdf"));
							
							document.open();
							document.add(new Paragraph("Example"));
							document.close();
						
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (DocumentException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						
						System.out.println("test = " + test.getComandaId());
					}
				});
				
				f1.add(add);
				f1.add(idC); f1.add(idP); f1.add(total);
				f1.add(n); f1.add(t); f1.add(a);
			}
		});
		
		*/
	
		
		
		frame.setSize(1000,800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
		frame.add(pane);frame.add(paneP);frame.add(paneO);
		frame.add(b1); frame.add(b2); frame.add(b3);
		
		
	}

	public void removeDinTabel(final DefaultTableModel model,int poz)
	{
		if(model.equals(modelC))
		{
			test.removeClientDB(Integer.parseInt((String) modelC.getValueAt(poz, 0)));
			modelC.removeRow(poz);
			populareTabele();
		}
		
		if(model.equals(modelP))
		{
			test.removeProdusDB(Integer.parseInt((String)modelP.getValueAt(poz, 0)));
			modelP.removeRow(poz);
			populareTabele();
		}
		
		if(model.equals(modelO))
		{
			test.removeComandaDB(Integer.parseInt((String)modelO.getValueAt(poz, 0)));
			modelO.removeRow(poz);
			populareTabele();
		}
	}
	
	public void populareTabele()
	{
		
		String[] a = {"1","2","3","4"};
		modelC.addRow(a);
		
		modelC.setRowCount(0);
		test.c.getClients();
		for(int i = 0; i < test.c.clienti.size(); i++)
		{
			String[] row = {String.valueOf(test.c.clienti.get(i).idClient),test.c.clienti.get(i).numeC,test.c.clienti.get(i).telefon,test.c.clienti.get(i).adresa};
			modelC.addRow(row);
		}
		
		modelP.setRowCount(0);
		test.p.getProduse();
		
		for(int i = 0; i < test.p.produse.size(); i++)
		{
			String[] row = {String.valueOf(test.p.produse.get(i).idProdus),test.p.produse.get(i).numeP,test.p.produse.get(i).cantitate,test.p.produse.get(i).pret};
			modelP.addRow(row);
		}
		
		
		modelO.setRowCount(0);
		test.o.getComenzi();
		
		for(int i = 0; i < test.o.comenzi.size(); i++)
		{
			String[] row = {String.valueOf(test.o.comenzi.get(i).idComanda),String.valueOf(test.o.comenzi.get(i).client),String.valueOf(test.o.comenzi.get(i).produs),test.o.comenzi.get(i).total};
			modelO.addRow(row);
		}
	}
	
	public ActionListener createAction(final DefaultTableModel model,final String s1,final String s2,final String s3,final String s0)
	{
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				temp++;
				new GUI();
				
				JFrame f1 = new JFrame("Add " + s0);
				f1.setSize(400,400);
				f1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				f1.setVisible(true);
				f1.setLayout(null);
				
				final JTextField idC;
				final JTextField idP;
				final JTextField total;
				JLabel n,t,a;
				JButton add;
				
				idC = new JTextField(); idP = new JTextField(); total = new JTextField();
				n = new JLabel(s1); t = new JLabel(s2); a = new JLabel(s3);
				add = new JButton("ADD");
				
				idC.setBounds(40,50,150,30); n.setBounds(40,20,100,30); n.setForeground(Color.black);
				idP.setBounds(40,150,150,30); t.setBounds(40,120,100,30); t.setForeground(Color.black);
				total.setBounds(40,250,150,30); a.setBounds(40,220,100,30); a.setForeground(Color.black);
			
				add.setBounds(250,200,100,30);
				add.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						//String[] rowt = {idC.getText(),idP.getText(),total.getText()};
						
						if(model.equals(modelC))
						{
							test.addClientDB(idC.getText(), idP.getText(), total.getText());
							populareTabele();
						}

						if(model.equals(modelP))
						{
							test.addProdusDB(idC.getText(), idP.getText(), total.getText());
							populareTabele();
						}

						if(model.equals(modelO))
						{
							
							
							int k; 
							
							for(int i = modelP.getRowCount() - 1; i>= 0; --i)
								for(int j = modelP.getColumnCount() - 1; j >= 0; --j)
									if(modelP.getValueAt(i, j).equals(idP.getText()))
									{
										int temp = Integer.parseInt(String.valueOf(modelP.getValueAt(i, 2))) - Integer.parseInt(total.getText());
										if(temp < 0)
										{
											System.out.println("not enough");
											
										}
										else
										{
										test.updateProdusDB(Integer.parseInt(idP.getText()), String.valueOf(modelP.getValueAt(i, 1)), String.valueOf(temp),String.valueOf(modelP.getValueAt(i, 3)));
										test.addComandaDB(Integer.parseInt(idP.getText()), Integer.parseInt(idC.getText()), total.getText());
										
										

										try {
											Document document = new Document();
											PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\user\\Desktop\\BDpb3\\pdftest\\test" + test.getComandaId() + ".pdf"));
											
											int last = modelO.getRowCount() - 1;
											int rowC = 0; int rowP = 0;
											
											for(int p = modelC.getRowCount() - 1; p>= 0; --p)
												for(int q = modelC.getColumnCount() - 1; q >= 0; --q)
													if(modelC.getValueAt(p,q).equals(modelO.getValueAt(last, 1)))
														rowC = p;
											
											for(int m = modelP.getRowCount() - 1;m>= 0; --m)
												for(int n = modelP.getColumnCount() - 1; n >= 0; --n)
													if(modelP.getValueAt(m, n).equals(modelO.getValueAt(last, 2)))
														rowP = m;
											
											document.open();
											document.add(new Paragraph("\t\t\t\tRecepit \n\n\n"));
											document.add(new Paragraph("Shipping to-->(name,phone,address) --->> " + modelC.getValueAt(rowC, 1) + " , " + modelC.getValueAt(rowC, 2) + " , " + modelC.getValueAt(rowC, 3)));
											document.add(new Paragraph("Description-->(name, quant, price) --->> " + modelP.getValueAt(rowP, 1) + " kg , " + modelO.getValueAt(last, 3) + " lei , " + modelP.getValueAt(rowP, 3)));
											document.add(new Paragraph("\n\n\t\t\t\t\t\t\t Total = " + ((Integer.parseInt((String) modelO.getValueAt(last, 3)) * Integer.parseInt((String)modelP.getValueAt(rowP, 3)))) + " lei"));
											document.close();
										
										} catch (FileNotFoundException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (DocumentException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										
										}
									}
							
							//test.updateProdusDB(Integer.parseInt(idP.getText()), String.valueOf(modelP.getValueAt(k, 1)), String.valueOf(modelP.getValueAt(k, 2)), String.valueOf(Integer.parseInt(String.valueOf(modelC.getValueAt(k, 3))) - Integer.parseInt(total.getText())));
							populareTabele();
							

							
						}
					}
				});
				
				
				f1.add(add);
				f1.add(idC); f1.add(idP); f1.add(total);
				f1.add(n); f1.add(t); f1.add(a);
				
			}
		};
		return al;
	}
}
