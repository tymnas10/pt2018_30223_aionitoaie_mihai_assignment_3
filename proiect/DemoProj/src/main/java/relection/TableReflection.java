package relection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Control.DB;
import Models.Client;
import Models.Produs;

public class TableReflection {
	JTable tableC;
	DB test = new DB();


	public ArrayList<String> displayField(Object obj) {

		ArrayList<String> ret = new ArrayList<String>();

		Class cl = obj.getClass();
		//System.out.println("No of fields = " + cl.getFields().length);

		for (java.lang.reflect.Field f : cl.getFields()) {
			ret.add(f.getName());
		}

		return ret;
	}

	public Object[][] displayDAta(List<?> obj) {
		DefaultTableModel modelC = new DefaultTableModel();
		
		Object[][] mat = new Object[obj.size()][100];
		int row = 0;int col = 0;
		
		test.c.getClients();
		ArrayList<Client> cl = new ArrayList<Client>();
		cl = test.c.clienti;
		
		String[] coll = displayField(new Produs()).toArray(new String[displayField(new Produs()).size()]);
		
		modelC.setColumnIdentifiers(coll);
		
		//if(obj.size() == 0)
		//{
		//	System.out.println("eroare");
		//}
		//else
		//{
		//System.out.println("No of fields = " + obj.get(0).getClass().getDeclaredFields().length);
		for (Object ob : obj) {
			ArrayList<String> t = new ArrayList<String>();
			
			for (java.lang.reflect.Field f : ob.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				Object value;
				
				try {
					
					value = f.get(ob);
					System.out.println(String.valueOf(value) + " " + row + " " + col);
					t.add(String.valueOf(value));
					mat[row][col] = value;
					
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			col++;	
			}
			col = 0;
			row++;
			
			
		}
		JTable tableC = new JTable(mat,displayField(new Client()).toArray(new String[displayField(new Client()).size()]));
		//}
		//return tableC;
		return mat;
	}
}
